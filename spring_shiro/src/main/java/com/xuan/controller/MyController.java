package com.xuan.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class MyController {

    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        return "hello world";
    }

    @RequestMapping({"/", "/index"})
    public String toIndex(Model model) {
        model.addAttribute("msg", "hello Spring Shiro");
        return "index";
    }

    @RequestMapping("user/add")
    public String add() {
        return "user/add";
    }

    @RequestMapping("user/edit")
    public String update() {
        return "user/edit";
    }

    @RequestMapping("toLogin")
    public String toLogin() {
        return "login";
    }

    @RequestMapping("/login")
    public String login(String username, String password, Model model) {
//        获取当前用户
        Subject subject = SecurityUtils.getSubject();
//        封装当前用户的登录数据
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        System.out.println("login中的token: " + token);
        System.out.println("执行login");
        try {
            subject.login(token); //执行登录的方法
        } catch (UnknownAccountException e) {
            model.addAttribute("msg", "用户名错误");
            return "login";

        } catch (IncorrectCredentialsException e) {
            model.addAttribute("msg", "密码错误");
            return "login";
        }
        return "index";
    }

    @RequestMapping("/nounth")
    @ResponseBody
    public String unauthorized() {
        return "此页面未授权";
    }

    @RequestMapping("/loginOut")
    public String loginOut(HttpSession httpSession, Model model) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        model.addAttribute("msg", "安全退出");
        return "login";
    }
}
