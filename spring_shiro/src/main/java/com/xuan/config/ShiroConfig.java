package com.xuan.config;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    /*
    subject 用户
    SecurityManager：管理所有用户
    Realm： 连接数据

     */
    //shiroFilterFactoryBean
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("getDefaultWebSecurityManager") DefaultWebSecurityManager defaultWebSecurityManager) {
        ShiroFilterFactoryBean filterFactoryBean = new ShiroFilterFactoryBean();
        filterFactoryBean.setSecurityManager(defaultWebSecurityManager);
        Map<String, String> map = new LinkedHashMap<>();
        /*
         * anon:无需认证就可以访问、
         * authc:必须认证了才能让问
         * user:必须拥有记住我功能才能用
         * perms :拥有对某个资源的权限才能访问;
         * roLe:拥有某个角色权限才能访问
         * */

        System.out.println("shiroFilterFactoryBean");
        map.put("/user/add", "anon");
        map.put("/user/edit", "authc");


        filterFactoryBean.setUnauthorizedUrl("/nounth");
        filterFactoryBean.setFilterChainDefinitionMap(map);
//        如果没有哦授权默认跳到login.jsp页面
        filterFactoryBean.setLoginUrl("/toLogin");
        return filterFactoryBean;
    }

    //DefaultWebSecurityManager
    @Bean
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm) {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(userRealm);
        System.out.println("DefaultWebSecurityManager");
        return securityManager;
    }


    //Realm 连接数据, 需要自定义
    @Bean
    public UserRealm userRealm() {
        System.out.println("userRealm");
        return new UserRealm();
    }


}
