package com.xuan.mapper;

import com.xuan.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Entity com.xuan.domain.User
 */
public interface UserMapper extends BaseMapper<User> {

}




