package com.xuan;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xuan.domain.User;
import com.xuan.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
@Slf4j
class MybatisPlusTestApplicationTests {

    @Resource
    private UserMapper userMapper;
    //使用pageHelper进行分页
    @Test
    void contextLoads() {
        PageHelper.startPage(1, 5);  //放在执行方法前面。
        List<User> users = userMapper.selectList(null);
        for (User u: users
             ) {
            log.info(u.toString());
        }
        log.info("..........................................");
    }

    @Test       //Ipage 进行分页
    void demo(){
        IPage<User> page = new Page<>(1, 5);
        IPage<User> page1 = userMapper.selectPage(page, null);
        List<User> records = page1.getRecords();
        for (User u: records
        ) {
            log.info(u.toString());
        }
    }

}
