package com.xuan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @GetMapping("login")
    public String home() {
        return "home";
    }

    @RequestMapping("/")
    public String index() {
        return "home";
    }

    @GetMapping("/role1")
    public String role1() {
        return "role1/role1";
    }

    @GetMapping("/role2")
    public String role2() {
        return "role2/role2";
    }
}
