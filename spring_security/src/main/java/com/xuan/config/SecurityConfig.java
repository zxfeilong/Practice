package com.xuan.config;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.xuan.dao.SysRoleDao;
import com.xuan.entity.SysRole;
import com.xuan.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleDao sysRoleDao;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 定制请求的授权规则
        // 首页所有人可以访问
        System.out.println("http...................................");
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/role1").hasRole("vip1")
                .antMatchers("/role2").hasRole("vip2")
                .and().formLogin();
    }

    //定义认证规则
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        System.out.println("auth...................................");
        //在内存中定义，也可以在jdbc中去拿....
        String[] s1 = {"vip1", "vip2"};
        auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
                .withUser("role1").password(new BCryptPasswordEncoder().encode("123")).roles("vip1")
                .and()
                .withUser("role2").password(new BCryptPasswordEncoder().encode("123")).roles("vip2")
                .and()
                .withUser("root").password(new BCryptPasswordEncoder().encode("123")).roles(s1);

//        auth.userDetailsService()

    }


    protected UserDetailsService userDetailsService() {
        return username -> {
            System.out.println(username);
            if (username == null) {
                throw new UsernameNotFoundException("用户名未找到");
            }
            QueryWrapper<SysRole> wrapper = Wrappers.query();
            wrapper.eq("role", "admin");
            return null;
        };
    }

}
