package com.xuan.util;

import lombok.Data;

@Data
public class Result {

    private int code;
    private Object data;
    private String msg;

    public static Result succ(int code, String msg, Object data) {
        Result r = new Result();
        r.setCode(code);
        r.setData(data);
        r.setMsg(msg);
        return r;
    }

    public static Result succ(Object data) {
        return Result.succ(200, "success", data);
    }

    public static Result succ(String msg) {
        return Result.succ(200, msg, null);
    }

    public static Result fail(int code, String msg, Object data) {
        Result r = new Result();
        r.setCode(code);
        r.setData(data);
        r.setMsg(msg);
        return r;
    }

    public static Result fail(String msg) {
        return Result.fail(500, msg, null);
    }
}
