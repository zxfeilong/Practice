package com.xuan.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (SysRole)实体类
 *
 * @author makejava
 * @since 2021-12-31 15:40:16
 */
@Data
public class SysRole implements Serializable {
    private static final long serialVersionUID = 499581142963106973L;
    
    private Integer id;
    /**
    * 角色
    */
    private String role;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}