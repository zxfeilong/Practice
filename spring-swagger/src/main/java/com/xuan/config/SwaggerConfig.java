package com.xuan.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/*
* 加上注解就可以使用
* 访问 http://localhost:8080/swagger-ui.html#/
* 由于版本问题使用 2.8的，以及在yml中添加  spring: mvc: pathmatch: matching-strategy: ANT_PATH_MATCHER
* */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
}
