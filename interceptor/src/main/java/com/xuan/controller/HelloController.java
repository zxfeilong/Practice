package com.xuan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class HelloController {

    @GetMapping("hello")
    @ResponseBody
    public String hello() {
        return "hello spring boot  you have login in";

    }


    @GetMapping("/")
    public String login() {
        return "login";
    }

    @GetMapping("/login")
    public String toLogin(String name, String pwd, HttpSession session) {

        System.out.println(name + " ........." + pwd);
        session.setAttribute("loginUser", name);
        return "redirect:/hello";
    }

}
